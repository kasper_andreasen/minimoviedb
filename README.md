# MiniMovieDB

3rd Java Assignment, creating a rest API using JPA and Hibernate.

<img src="https://ibb.co/QK03yV1"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![web](https://img.shields.io/static/v1?logo=heroku&message=Online&label=Heroku&color=430098)](https://otunes-spring-app.herokuapp.com/)


- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Online Deployment](#online-deployment)
- [Maintainers](#maintainers)
- [License](#license)

## Background

This Spring applicaiton is concerned with creating and exposing our own database, generated with Hibernation and JPA, as ab API using the Spring Boot Framework.

The application consists of the following:

- A basic Rest Controller with one endpoint (`/`) which produces a JSON output.
- An OpenAPI configuration endpoint at `/.well-known/oas`. The path for this is configured in `./src/main/resources/application.properties`. This is also not a "real" `.well-known` endpoint, however it probably will be at some point and this puts it out of the way. Without specifying this, the path is set to `/v3/api-docs`.
- In leau of doing the Postman API call documentation, we opted to implement OpenAPI and Swagger-UI.
- The Swagger documentation UI can be found at `https://tiny-movie-app.herokuapp.com/swagger-ui/index.html`.

### A Primer on URIs

The API endpoints for the `FranchiseContoller` looks like this:

```
GET https://tiny-movie-app.herokuapp.com/franchise/get/all
GET https://tiny-movie-app.herokuapp.com/franchise/findFirstByNameContaining/{franchiseName}
GET https://tiny-movie-app.herokuapp.com/franchise/findById/{franchiseId}
PUT https://tiny-movie-app.herokuapp.com/franchise/franchise/{id} // Replace existing
PATCH https://tiny-movie-app.herokuapp.com/franchise/franchise/{id} // Update existing
POST https://tiny-movie-app.herokuapp.com/franchise/create/franchise
DELETE https://tiny-movie-app.herokuapp.com/franchise/delete/{id}

```
The API endpoints for the `CharacterController` looks like this:

```
GET https://tiny-movie-app.herokuapp.com/character/get/all
GET https://tiny-movie-app.herokuapp.com/character/findFirstByNameContaining/{characterName}
GET https://tiny-movie-app.herokuapp.com/character/findFirstByAliasContaining/{alias}
GET https://tiny-movie-app.herokuapp.com/character/findById/{characterId}
PUT https://tiny-movie-app.herokuapp.com/character/{characterId}/movie/{movieId} // Assign character to a movie
PUT https://tiny-movie-app.herokuapp.com/character/{characterId}/franchise/{franchiseId} // Assign character to a franchise
PATCH https://tiny-movie-app.herokuapp.com/character/character/{id} // Update Existing
POST https://tiny-movie-app.herokuapp.com/character/create/character
DELETE https://tiny-movie-app.herokuapp.com/character/delete/{id}
```

The API endpoints for the `MovieController`looke like this:

```
GET https://tiny-movie-app.herokuapp.com/movie/get/all
GET https://tiny-movie-app.herokuapp.com/movie/findFirstByTitleContaining/{movieTitle}
GET https://tiny-movie-app.herokuapp.com/movie/findbById/{movieId}
PUT https://tiny-movie-app.herokuapp.com/movie/{movieId}/characters/{characterId} // Assign character to movie
PATCH https://tiny-movie-app.herokuapp.com/movie/update/{movieId}
POST https://tiny-movie-app.herokuapp.com/movie/create/movie
DELETE https://tiny-movie-app.herokuapp.com/movie/delete/{id}

```

## Install

Gradle will automatically initialize itself and download necessary dependencies the first time the wrapper is run. No explicit installation necessary.

## Online-Deployment

A live version of this project is deployed on Heroku and can be found at https://tiny-movie-app.herokuapp.com/

## Maintainers

[Kasper Gade Andreasen (@kasper_andreasen)](https://gitlab.com/kasper_andreasen)

[Allan Theilgaard Carlsen (@AllanCarlsen)](https://gitlab.com/AllanCarlsen)


## License

This project is licensed under the MIT license.
