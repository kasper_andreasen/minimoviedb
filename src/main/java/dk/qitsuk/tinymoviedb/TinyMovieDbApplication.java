package dk.qitsuk.tinymoviedb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TinyMovieDbApplication {

    public static void main(String[] args) {
        SpringApplication.run(TinyMovieDbApplication.class, args);
    }
}
