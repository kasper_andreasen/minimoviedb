package dk.qitsuk.tinymoviedb.controller;

import dk.qitsuk.tinymoviedb.models.Character;
import dk.qitsuk.tinymoviedb.models.Franchise;
import dk.qitsuk.tinymoviedb.models.Movie;
import dk.qitsuk.tinymoviedb.models.responsUtils.CommonResponse;
import dk.qitsuk.tinymoviedb.repositories.CharacterRepository;
import dk.qitsuk.tinymoviedb.repositories.FranchiseRepository;
import dk.qitsuk.tinymoviedb.repositories.MovieRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@Tag(name = "Character")
@RequestMapping("/character")
public class CharacterController {
    // TODO: Chore: Create a service for this instead.
    final CharacterRepository characterRepository;
    final MovieRepository movieRepository;
    final FranchiseRepository franchiseRepository;

    public CharacterController(CharacterRepository characterRepository, MovieRepository movieRepository, FranchiseRepository franchiseRepository) {
        this.characterRepository = characterRepository;
        this.movieRepository = movieRepository;
        this.franchiseRepository = franchiseRepository;
    }

    @GetMapping("/")
    @Operation(summary = "Test character controller")
    public ResponseEntity<CommonResponse> index() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(new CommonResponse<>("This is the RestController for character"));
    }

    @GetMapping("/get/all")
    @Operation(summary = "Get all characters in DB")
    public ResponseEntity<CommonResponse> getAllCharacters() {
        try {
            List<Character> result = characterRepository.findAll();
            if (result.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body(new CommonResponse<>("No characters in DB?!"));
            }
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new CommonResponse<>(result));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new CommonResponse<>("Something went wrong. Working on it." +
                            e.getMessage()));
        }
    }

    @GetMapping("/findFirstByNameContaining/{characterName}")
    @Operation(summary = "Find first character where name contains @PathVariable")
    public ResponseEntity<CommonResponse> findFirstByNameContaining(@PathVariable String characterName) {
        try {
            Character character = characterRepository.findFirstByNameContaining(characterName);
            if (character == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body(new CommonResponse<>("No Characters with that name found"));
            }
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new CommonResponse<>(character));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new CommonResponse<>("Something went wrong. Working on it." +
                            e.getMessage()));
        }
    }

    @GetMapping("/findFirstByAliasContaining/{alias}")
    @Operation(summary = "Find first character where alias contains @PathVariable")
    public ResponseEntity<CommonResponse> findFirstByAliasContaining(@PathVariable String alias) {
        try {
            Character character = characterRepository.findFirstByAliasContaining(alias);
            if (character == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body(new CommonResponse<>("No Characters with that alias found"));
            }
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new CommonResponse<>(character));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new CommonResponse<>("Something went wrong. Working on it." +
                            e.getMessage()));
        }
    }

    @GetMapping("findById/{characterId}")
    @Operation(summary = "Find character by Id as @PathVariable")
    public ResponseEntity<CommonResponse> findById(@PathVariable int characterId) {
        try {
            Optional<Character> result = characterRepository.findById(characterId);
            if (result.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body(new CommonResponse<>("No character found with id: " + characterId));
            }
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new CommonResponse<>(result));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new CommonResponse<>("Something went wrong. Working on it." +
                            e.getMessage()));
        }
    }

    @PostMapping("/create/character")
    @Operation(summary = "Create a new character")
    public ResponseEntity<CommonResponse> createCharacter(@RequestBody Character newCharacter) {
        try {
            Character character = characterRepository.save(newCharacter);
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(new CommonResponse<>("Character created with id: " + character.getId()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new CommonResponse<>("Something went wrong. Working on it. " +
                            e.getMessage()));
        }
    }

    @DeleteMapping("/delete/{id}")
    @Operation(summary = "Delete character with id equals to @PathVariable")
    public ResponseEntity<CommonResponse> deleteCharacterById(@PathVariable int id) {
        try {
            Optional <Character> result = characterRepository.findById(id);
            if (result.isPresent()) {
                Character character = result.get();
                character.setFranchise(null);
                characterRepository.save(character);
            }

            characterRepository.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new CommonResponse<>("The character with id " + id + " was deleted"));
        } catch (Exception e) {
            // Returns OK (instead of NOT_FOUND) to signal that everything is as expected.
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new CommonResponse<>("The requested entity was already deleted"));
        }
    }

    @PatchMapping("/character/{id}")
    @Operation(summary = "Update an existing character by id")
    public ResponseEntity<CommonResponse<String>> updateCharacter(@RequestBody Character newCharacter, @PathVariable int id) {
        try {
            if (characterRepository.existsById(id)) {
                Optional<Character> characterRepo = characterRepository.findById(id);
                if (characterRepo.isPresent()) {
                    Character character = characterRepo.get();
                    if (newCharacter.getName() != null) {
                        character.setName(newCharacter.getName());
                    }
                    if (newCharacter.getAlias() != null) {
                        character.setName(newCharacter.getAlias());
                    }
                    if (newCharacter.getGender() != null) {
                        character.setName(newCharacter.getGender());
                    }
                }
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new CommonResponse<>("Character with id " + id + " updated."));
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body(new CommonResponse<>("No character found with id " + id + ". Did nothing."));
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(new CommonResponse<>("The requested entity was already deleted or never existed"));
        }
    }

    @PutMapping("/{characterId}/movie/{movieId}")
    @Operation(summary = "Assign a movie to character")
    public ResponseEntity<CommonResponse> assignMovieToCharacter(
            @PathVariable int characterId,
            @PathVariable int movieId
    ){
        try{
            Optional<Character> character = characterRepository.findById(characterId);
            Optional<Movie> movie = movieRepository.findById(movieId);
            if(character.isPresent() && movie.isPresent()){
                character.get().addMovie(movie.get());
                return ResponseEntity.status(HttpStatus.CREATED).body(new CommonResponse<>(characterRepository.save(character.get())));
            } else if(character.isPresent()){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CommonResponse<>("MovieId: " + movieId + " does not exist yet."));
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CommonResponse<>("CharacterId: " + characterId + " does not exist yet."));
            }
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CommonResponse<>(e.getMessage()));
        }
    }

    @PutMapping("/{characterId}/franchise/{franchiseId}")
    @Operation(summary = "Assign a franchise to character")
    public ResponseEntity<CommonResponse> assignFranchiseToCharacter(
            @PathVariable int characterId,
            @PathVariable int franchiseId
    ){
        try{
            Optional<Character> character = characterRepository.findById(characterId);
            Optional<Franchise> franchise = franchiseRepository.findById(franchiseId);
            if(character.isPresent() && franchise.isPresent()){
                character.get().setFranchise(franchise.get());
                return  ResponseEntity.status(HttpStatus.CREATED).body(new CommonResponse<>(characterRepository.save(character.get())));
            } else if(character.isPresent()){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CommonResponse<>("FranchiseId: " + franchiseId + " does not exist yet."));
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CommonResponse<>("CharacterId: " + characterId + " does not exist yet."));
            }
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CommonResponse<>(e.getMessage()));
        }
    }
}
