package dk.qitsuk.tinymoviedb.controller;

import dk.qitsuk.tinymoviedb.models.Franchise;
import dk.qitsuk.tinymoviedb.models.responsUtils.CommonResponse;
import dk.qitsuk.tinymoviedb.repositories.FranchiseRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@Tag(name = "Franchise")
@RequestMapping("/franchise")
public class FranchiseController {
    private final FranchiseRepository franchiseRepository;

    public FranchiseController(FranchiseRepository franchiseRepository) {
        this.franchiseRepository = franchiseRepository;
    }

    @GetMapping("/")
    @Operation(summary = "Test Franchise controller")
    public ResponseEntity<CommonResponse<String>> index() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(new CommonResponse<>("This is the RestController for franchise"));
    }

    @GetMapping("/get/all")
    @Operation(summary = "Get all franchises in DB")
    public ResponseEntity<CommonResponse> getAllFranchises() {
        try {
            List<Franchise> result = franchiseRepository.findAll();
            if (result.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body(new CommonResponse<>("No franchises found in DB"));
            }
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new CommonResponse<>(result));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new CommonResponse<>("Something went wrong. Working on it. "
                    + e.getMessage()));
        }
    }
    @GetMapping("/findFirstByNameContaining/{franchiseName}")
    @Operation(summary = "Find first franchise where name contains @PathVariable")
    public ResponseEntity<CommonResponse> findFirstByNameContaining(@PathVariable String franchiseName) {
        try {
            Franchise franchise = franchiseRepository.findFirstByNameContaining(franchiseName);
            if (franchise == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body(new CommonResponse<>("No franchises found with that name"));
            }
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new CommonResponse<>(franchise));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new CommonResponse<>("Something went wrong. Working on it. "
                    +e.getMessage()));
        }
    }
    @GetMapping("/findById/{franchiseId}")
    @Operation(summary = "Find franchise by Id as @PathVariable")
    public ResponseEntity<CommonResponse> findById(@PathVariable int franchiseId) {
        try {
            Optional<Franchise> result = franchiseRepository.findById(franchiseId);
            if (result.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body(new CommonResponse<>("No franchise found with that id."));
            }
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new CommonResponse<>(result));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new CommonResponse<>("Something went wrong. Working on it. "
                            +e.getMessage()));
        }
    }

    @PostMapping("/create/franchise")
    @Operation(summary = "Create a new Franchise")
    public ResponseEntity<CommonResponse> createFranchise(@RequestBody Franchise newFranchise) {
        try {
            Franchise franchise = franchiseRepository.save(newFranchise);
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(new CommonResponse<>("Created a new Franchise with id " + franchise.getId()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new CommonResponse<>("Something went wrong. Working on it. "
                            +e.getMessage()));
        }
    }


    @PatchMapping("/franchise/{id}")
    @Operation(summary = "Update an existing Franchise by id")
    public ResponseEntity<CommonResponse> updateFranchise(@RequestBody Franchise newFranchise, @PathVariable int id) {
        try {
            if (franchiseRepository.existsById(id)) {
                Optional<Franchise> result = franchiseRepository.findById(id);
                if (result.isPresent()) {
                    Franchise franchise = result.get();
                    if (newFranchise.getName() != null) {
                        franchise.setName(newFranchise.getName());
                    }
                    if (newFranchise.getDescription() != null) {
                        franchise.setDescription(newFranchise.getDescription());
                    }
                    franchiseRepository.save(franchise);
                }
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new CommonResponse<>("Franchise with id " + id + " updated."));
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body(new CommonResponse<>("No franchise with id " + id + " found. Did nothing."));
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(new CommonResponse<>("The requested entity was already deleted or never existed"));
        }
    }

    @PutMapping("/franchise/{id}")
    @Operation(summary = "Replace an existing franchise by id")
    public ResponseEntity<CommonResponse> replaceFranchise(@RequestBody Franchise newFranchise, @PathVariable int id) {
        try {
            if (franchiseRepository.existsById(id)) {
                Optional<Franchise> result = franchiseRepository.findById(id);
                if (result.isPresent()) {
                    Franchise franchise = result.get();
                    franchise.setName(newFranchise.getName());
                    franchise.setDescription(newFranchise.getDescription());
                    franchiseRepository.save(franchise);
                }
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new CommonResponse<>("Franchise with id " + id + " replaced."));
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body(new CommonResponse<>("No franchise found with id " + id + ". Did nothing."));
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new CommonResponse<>("Something went wrong. Working on it. "
                    + e.getMessage()));
        }
    }
    @DeleteMapping("/delete/{id}")
    @Operation(summary = "Delete a franchise by id")
    public ResponseEntity<CommonResponse> deleteFranchise(@PathVariable int id) {
        try {
            franchiseRepository.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new CommonResponse<>("Franchise with id " + id + " deleted."));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new CommonResponse<>("Franchise with id " + id + " was already deleted" +
                            "or never existed. Did nothing!"));
        }
    }
}
