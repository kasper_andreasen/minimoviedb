package dk.qitsuk.tinymoviedb.controller;

import dk.qitsuk.tinymoviedb.models.Character;
import dk.qitsuk.tinymoviedb.models.Movie;
import dk.qitsuk.tinymoviedb.models.responsUtils.CommonResponse;
import dk.qitsuk.tinymoviedb.repositories.CharacterRepository;
import dk.qitsuk.tinymoviedb.repositories.MovieRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.api.OpenApiResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@Tag(name = "Movie")
@RequestMapping("/movie")
public class MovieController {

    // Making MovieRepository available for controller.
    private final MovieRepository movieRepository;
    private final CharacterRepository characterRepository;

    public MovieController(MovieRepository movieRepository, CharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }

    @GetMapping("/")
    @Operation(summary = "Test movie controller")
    public ResponseEntity<CommonResponse> index() {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new CommonResponse<>("This is the RestController for movies"));
    }

    @GetMapping("/get/all")
    @Operation(summary = "Get all movies in DB")
    public ResponseEntity<CommonResponse> getAllMovies() {
        List<Movie> result = movieRepository.findAll();
        if(result.isEmpty()) return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new CommonResponse<>("No movies in DB!?"));
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new CommonResponse<>(result));
    }

    @GetMapping("/findFirstByTitleContaining/{movieTitle}")
    @Operation(summary = "Find first movie containing @PathVariable")
    public ResponseEntity<CommonResponse> findFirstByTitleContaining(@PathVariable String movieTitle) {
        try {
            Movie movie = movieRepository.findFirstByTitleContaining((movieTitle));
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(new CommonResponse<>(movie));
        } catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(new CommonResponse<>(e.getMessage()));
        }

    }

    @GetMapping("/findById/{movieId}")
    @Operation(summary = "Find movie by id as @PathVariable")
    public ResponseEntity<CommonResponse> findById(@PathVariable int movieId) {

        Optional<Movie> result = movieRepository.findById(movieId);

        if (result.isEmpty()) return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new CommonResponse<>("No movie with id: " + movieId));

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new CommonResponse<>(result));
    }

    @PostMapping("/create/movie")
    @Operation(summary = "Create new movie")
    public ResponseEntity<CommonResponse> createMovie(@RequestBody Movie newMovie) {

        try{
            Movie movie = movieRepository.save(newMovie);
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(new CommonResponse<>("Movie created with ID: " + movie.getId()));
        } catch (Exception e){
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(new CommonResponse<>(e.getMessage()));
        }
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<CommonResponse> deleteMovieById(@PathVariable int id) {

        try{
            Optional<Movie> result = movieRepository.findById(id);
            if (result.isPresent()) {
                Movie movie = result.get();
                movie.setFranchise(null);
                movieRepository.save(movie);
            }
            movieRepository.deleteById(id);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(new CommonResponse<>("Deleted"));

        }catch (Exception e){
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(new CommonResponse<>("The requested entity was already deleted"));
        }
    }

    @PatchMapping(value = "/update/{movieId}")
    public ResponseEntity<CommonResponse> updateMovieById(@PathVariable int movieId, @RequestBody Movie movieDetails) throws OpenApiResourceNotFoundException {

        Movie movie = movieRepository.findById(movieId)
                .orElseThrow(() -> new OpenApiResourceNotFoundException("Movie with ID: " + movieId + " was not found!"));

        movie.setTitle(movieDetails.getTitle());
        movie.setGenre(movieDetails.getGenre());
        movie.setReleaseYear(movieDetails.getReleaseYear());
        movie.setDirector(movieDetails.getDirector());

        final Movie updatedMovie = movieRepository.save(movie);

        return ResponseEntity.status(HttpStatus.CREATED).body(new CommonResponse<>(updatedMovie));
    }

    @PutMapping("/{movieId}/characters/{characterId}")
    @Operation(summary = "Assign a character to a movie.")
    public ResponseEntity<CommonResponse> assignCharacterToMovie(
            @PathVariable int movieId,
            @PathVariable int characterId
    ){
        try{
            Optional<Movie> movie = movieRepository.findById(movieId);
            Optional<Character> character = characterRepository.findById(characterId);
            if(movie.isPresent() && character.isPresent()){
                movie.get().addCharacter(character.get());
                return ResponseEntity.status(HttpStatus.CREATED).body(new CommonResponse<>(movieRepository.save(movie.get())));
            } else if(movie.isPresent()){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CommonResponse<>("CharacterId: " + characterId + " does not exist yet."));
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new CommonResponse<>("MovieId: " + movieId + " does not exist yet."));
            }
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CommonResponse<>(e.getMessage()));
        }
    }
}


