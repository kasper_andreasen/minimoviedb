package dk.qitsuk.tinymoviedb.controller;

import dk.qitsuk.tinymoviedb.models.responsUtils.CommonResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = "Root")
public class RootController {
    @GetMapping("/")
    public ResponseEntity<CommonResponse> getApiInfo() {
        return ResponseEntity
                .ok()
                .body(new CommonResponse(-1, "No API info yet"));
    }
}
