package dk.qitsuk.tinymoviedb.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length= 50, nullable = false, unique = true)
    private String name;
    @Column(length = 50, unique = true)
    private String alias;
    // Setting the length of gender to one, as gender should be defined by a single letter, I.E. M/F
    @Column(length = 1, nullable = false)
    private String gender;


    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "franchise_id", referencedColumnName = "id")
    private Franchise franchise;

    @JsonGetter("movies")
    public List<String> movies() {
        return movies.stream().map(movie -> {
            return "/movie/" + movie.getId();
        }).collect(Collectors.toList());
    }
    @ManyToMany
    @JoinTable(
            name = "movie_character",
            joinColumns = @JoinColumn(name = "character_id"),
            inverseJoinColumns = @JoinColumn(name = "movie_id")
    )
    private List<Movie> movies = new ArrayList<>();


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAlias() {
        return alias;
    }

    public String getGender() {
        return gender;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void addMovie(Movie movie) {
        movies.add(movie);
    }
    @JsonGetter("franchise")
    public String getFranchise() {
        return "/franchise/" + franchise.getId();
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

}
