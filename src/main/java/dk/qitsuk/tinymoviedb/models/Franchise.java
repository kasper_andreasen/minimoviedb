package dk.qitsuk.tinymoviedb.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50, nullable = false)
    private String name;
    @Column(columnDefinition = "TEXT", nullable = false, length = 1000)
    private String description;



    @OneToMany(mappedBy = "franchise")
    private List<Movie> movies = new ArrayList<>();


    @OneToMany(mappedBy = "franchise")
    private List<Character> characters = new ArrayList<>();

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
    @JsonGetter("movies")
    public List<String> movie() {
        return movies.stream().map(movie -> {
            return "/movie/" + movie.getId();
        }).collect(Collectors.toList());
    }

    @JsonGetter("characters")
    public List<String> character() {
        return characters.stream().map(character -> {
            return "/character/" + character.getId();
        }).collect(Collectors.toList());
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }
}
