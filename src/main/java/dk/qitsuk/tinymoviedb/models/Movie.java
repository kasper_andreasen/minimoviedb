package dk.qitsuk.tinymoviedb.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private String title;

    @Column(length = 50, nullable = false)
    private String genre;

    @Column(name = "release_year", nullable = false)
    private int releaseYear;

    @Column(length = 30, nullable = false)
    private String director;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "franchise_id", referencedColumnName = "id")
    private Franchise franchise;

    @ManyToMany(mappedBy = "movies")
    private List<Character> characters = new ArrayList<>();


    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public String getDirector() {
        return director;
    }


    public void setTitle(String title) {
        this.title = title;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    @JsonGetter("characters")
    public List<String> characters() {
        return characters.stream().map(character -> {
            return "/character/" + character.getId();
        }).collect(Collectors.toList());
    }

    public void addCharacter(Character character) {
        characters.add(character);
    }
    @JsonGetter("franchise")
    public String getFranchise() {
        return "/franchise/" + franchise.getId();
    }
}
