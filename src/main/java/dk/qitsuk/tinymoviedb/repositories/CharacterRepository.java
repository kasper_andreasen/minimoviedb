package dk.qitsuk.tinymoviedb.repositories;

import dk.qitsuk.tinymoviedb.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CharacterRepository extends JpaRepository<Character, Integer> {
    Character findFirstByNameContaining(String name);
    Character findFirstByAliasContaining(String alias);
}
