package dk.qitsuk.tinymoviedb.repositories;

import dk.qitsuk.tinymoviedb.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FranchiseRepository extends JpaRepository<Franchise, Integer> {
    Franchise findFirstByNameContaining(String franchiseName);
}
