package dk.qitsuk.tinymoviedb.repositories;

import dk.qitsuk.tinymoviedb.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie, Integer> {
    Movie findFirstByTitleContaining(String title);
}
